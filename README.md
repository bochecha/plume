Plume is a library which can be used in command-line tools to let users write
text in their favourite editor.

Think about how Git spawns your ${EDITOR} to let you write a commit message.

# Usage

```rust
extern crate plume;

use plume::get_text;

let text = get_text()?;
```

The above will:

1.  find the text editor to use
    * if the `${EDITOR}` environment variable is set, then its value is used;
    * otherwise, Plume will search for known text editors like `nano` or `vim`;

2. launch that text editor and capture the text entered by the user;

3. return that text.

# License

Plume is offered under the terms of the
[GNU Lesser General Public License, either version 3 or any later version](http://www.gnu.org/licenses/lgpl.html).
